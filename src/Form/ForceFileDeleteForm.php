<?php

namespace Drupal\forcedfiledeletion\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file_delete\Form\FileDeleteForm;

/**
 * Add support for forcibly deleting a file to the file deletion support.
 */
class ForceFileDeleteForm extends FileDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $usages = $this->fileUsage->listUsage($this->entity);
    if (!$usages || !\Drupal::currentUser()->hasPermission('forcibly delete a file')) {
      return parent::getQuestion();
    }

    return $this->t('This file has existing use. The file itself (%file_path) will be immediately deleted? References to it will be left untouched (and should be unpublished) but download links will stop working.', [
      '%file_name' => $this->entity->getFilename(),
      '%file_path' => $this->entity->getFileUri(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    $usages = $this->fileUsage->listUsage($this->entity);
    if (!$usages || !\Drupal::currentUser()->hasPermission('forcibly delete a file')) {
      return parent::getConfirmText();
    }

    return $this->t('Forcibly Delete File');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $usages = $this->fileUsage->listUsage($this->entity);
    if (!$usages || !\Drupal::currentUser()->hasPermission('forcibly delete a file')) {
      return parent::submitForm($form, $form_state);
    }

    // Immediately delete the target file.
    $uri = $this->entity->getFileUri();

    $filesystem = \Drupal::service('file_system');
    $realPath = $filesystem->realpath($uri);
    if ($realPath && file_exists($realPath)) {
      unlink($realPath);
    }

    $this->messenger()->addMessage($this->t('The file %file_name has been deleted from the filesystem.', [
      '%file_name' => $this->entity->getFilename(),
    ]));

    // Clear cache tag for the file.
    \Drupal::service('cache_tags.invalidator')->invalidateTags($this->entity->getCacheTags());

    $form_state->setRedirect('view.files.page_1');
  }

}
